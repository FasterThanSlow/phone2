<?php
	$message = "";
	
	if(isset($_GET["send"])){
		if(!empty($_GET["phone"])){
			$phone = $_GET["phone"];
			$regExp = "/(\d|\+)?\(?\d+\)?\d+[ -]?\d+[ -]?\d+$/";
			$count = preg_match_all("/\d{1}/",$phone);
			if(@preg_match($regExp,$phone) == 1 && $count >= 11){
				$message = "Введен корректный номер телефона.";
			}
			else{
				$message = "Введен не корректный номер телефона.";
			}
		}
	}
?>

<html>
	<head>
		<title>Проверка на валидность телефона</title>
	</head>
	<body>
		<div class="wrap">
		<form action="<?=$_SERVER['PHP_SELF']?>" method="get">
			<p>Введите телефон: </p>
			<p><input type="text" name="phone"></p>
			<p><input type="submit" name="send" value="Проверить телефон"></p>
			<p><?=$message?></p>
		</form>
		</div>
	</body>
</html>